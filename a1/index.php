<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 1</title>
</head>
<body>
<h1>Full Address</h1>

<p><?php echo getFullAddress('The Philippines', 'Caloocan', 'NCR', 'New Born') ?></p>
<p><?php echo getFullAddress('The Philippines', 'Makati', 'NCR', 'Phase 33 Package 10 Block 90 Lot 23') ?></p>

<p><?php echo getLetterGrade(95)?></p>

<p><?php echo getLetterGrade(85)?></p>

<p><?php echo getLetterGrade(75)?></p>

</body>
</html>